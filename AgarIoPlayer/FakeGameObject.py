 from Point import Point


class FakeGameObject:

    def __init__(self, mass, x, y, contour):
        self.mass = mass
        self.attractiveness = 0
        self.x = x
        self.y = y
        self.contour = contour
        self.is_zersplitterer = False

    def get_middle(self):
        return Point(self.x, self.y)
