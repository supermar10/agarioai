import random
import time

import cv2
import imutils
import numpy as np
import pyautogui

import FrameGrabbing
from FakeGameObject import FakeGameObject
from GameObject import GameObject


def is_zersplitter_dingens(own_object, object_to_test):
    # Verhaeltnis von eigener Masse zu zersplitter dings masse verwenden
    try:
        ratio = own_object.mass / object_to_test.mass
    except:
        ratio = 0
    return 2.8 < ratio < 2.84
    return 35900 < object_to_test.mass < 37000


def calculate_distance(from_object_middle, to_object_middle):
    return np.sqrt(
        np.square(from_object_middle.x - to_object_middle.x) + np.square(from_object_middle.y - to_object_middle.y))


def get_objects_in_radius(point, all_game_objects, radius):
    game_objects_in_radius = []
    for game_object in all_game_objects:
        if calculate_distance(point, game_object.get_middle()) < radius:
            game_objects_in_radius.append(game_object)
    return game_objects_in_radius


def get_danger_in_area(own_object, game_object, all_game_objects):
    objects_in_radius = get_objects_in_radius(game_object.get_middle(), all_game_objects, 700)
    if len(objects_in_radius) == 0:
        return 0

    danger = 0
    for object_in_radius in objects_in_radius:
        if object_in_radius.mass >= own_object.mass * 1.05:
            distance = calculate_distance(game_object.get_middle(), object_in_radius.get_middle())

            danger += 1 * 1.2 * object_in_radius.mass / own_object.mass

            if distance > 0:
                danger *= 100 / distance

            # print("Distance: " + str(calculate_distance(game_object.get_middle(), object_in_radius.get_middle())))

        if is_zersplitter_dingens(own_object, object_in_radius):
            print("zersplitterer")
            danger *= 100000

    # print("Danger: " + str(danger))
    # print("Own_mass: " + str(own_object.mass))
    return danger


def get_attractiveness_of_object(own_object, game_object, all_game_objects):
    mean_mass = 0
    mean_distance = 0
    for all_game_object in all_game_objects:
        mean_mass += all_game_object.mass
        mean_distance += calculate_distance(game_object.get_middle(), all_game_object.get_middle())

    mean_mass /= len(all_game_objects)
    mean_distance /= len(all_game_objects)

    danger = get_danger_in_area(own_object, game_object, all_game_objects)

    if game_object.mass <= own_object.mass * 0.95:
        attractiveness = 5 * (game_object.mass / own_object.mass) - np.square(6 * danger) + 15 / (
                calculate_distance(own_object.get_middle(), game_object.get_middle()) / mean_distance)
    else:
        attractiveness = -100000000

    return attractiveness


def get_mouse_direction(own_object, other_objects, image_paint_on):
    maximum_mouse_movement = 100 * own_object.mass / 4400
    if maximum_mouse_movement > 300:
        maximum_mouse_movement = 300

    maximum_mouse_movement = int(maximum_mouse_movement)
    smaller_objects = []
    bigger_objects = []

    for other_object in other_objects:
        if own_object is not None:
            other_object.attractiveness = get_attractiveness_of_object(own_object, other_object, other_objects)

            if other_object.mass < (own_object.mass * 0.95):
                smaller_objects.append(other_object)
            else:
                bigger_objects.append(other_object)

            cv2.drawContours(image_paint_on, [own_object.contour], -1, (255, 0, 0), 2)
            # cv2.putText(image_paint_on, "{:+.4f}".format(own_object.attractiveness),
            #             (int(own_object.get_middle().x), int(own_object.get_middle().y)),
            #             cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

        cv2.drawContours(image_paint_on, [other_object.contour], -1, (0, 255, 0), 2)
        cv2.putText(image_paint_on, "{:+.4f}".format(other_object.attractiveness),
                    (int(other_object.get_middle().x), int(other_object.get_middle().y)),
                    cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)

    cv2.imshow("objects", image_paint_on)
    cv2.waitKey(1)

    if len(smaller_objects) > 0:
        most_attractive_object = smaller_objects[0]
        for smaller_object in smaller_objects:
            if smaller_object.attractiveness != 0 and smaller_object.attractiveness > most_attractive_object.attractiveness:
                most_attractive_object = smaller_object

        most_attractive_middle = most_attractive_object.get_middle()
        own_middle = own_object.get_middle()

        distance_x = most_attractive_middle.x - own_middle.x
        distance_y = most_attractive_middle.y - own_middle.y

        mouse_offset_x = (distance_x / calculate_distance(own_object.get_middle(),
                                                          most_attractive_object.get_middle())) * maximum_mouse_movement
        mouse_offset_y = (distance_y / calculate_distance(own_object.get_middle(),
                                                          most_attractive_object.get_middle())) * maximum_mouse_movement
    else:
        # print("Choosing random movement")
        mouse_offset_x = random.randint(0, maximum_mouse_movement)
        mouse_offset_y = random.randint(0, maximum_mouse_movement)
        most_attractive_object = None

    return mouse_offset_x, mouse_offset_y, most_attractive_object


def get_objects(image_to_process):
    height, width, depth = image_to_process.shape

    lower_normal_object = np.array([0, 0, 0])
    upper_normal_object = np.array([220, 220, 220])
    shape_mask_normal_object = cv2.inRange(image_to_process, lower_normal_object, upper_normal_object)


    # cv2.imshow("objects", image_to_process)
    # cv2.waitKey(0)

    # RBG
    lower_zersplitter_object = np.array([50, 180, 50])
    upper_zersplitter_object = np.array([55, 255, 60])

    # lower_zersplitter_object = np.array([110, 70, 90])
    # upper_zersplitter_object = np.array([130, 90, 100])
    # hsv = cv2.cvtColor(image_to_process, cv2.COLOR_BGR2HSV)

    shape_mask_zersplitter_object = cv2.inRange(image_to_process, lower_zersplitter_object, upper_zersplitter_object)

    contours = cv2.findContours(shape_mask_zersplitter_object.copy(), cv2.RETR_EXTERNAL,
                                cv2.CHAIN_APPROX_SIMPLE)
    contours = imutils.grab_contours(contours)

    # shape_mask_zersplitter_object = cv2.cvtColor(shape_mask_zersplitter_object, cv2.COLOR_GRAY2BGR)

    res = cv2.bitwise_and(image_to_process, image_to_process, mask=shape_mask_zersplitter_object)

    cv2.drawContours(res, contours, -1, (255, 0, 0), 2)
    cv2.imshow("objects", res)
    cv2.waitKey(1)

    contours = cv2.findContours(shape_mask_normal_object.copy(), cv2.RETR_EXTERNAL,
                                cv2.CHAIN_APPROX_SIMPLE)
    contours = imutils.grab_contours(contours)
    own_object = None
    other_objects = []
    for contour in contours:
        # draw the contour and show it
        # cv2.drawContours(img, [c], -1, (0, 255, 0), 2)

        game_object = GameObject(cv2.contourArea(contour), contour)

        is_self = cv2.pointPolygonTest(contour, (width / 2, height / 2), True) >= 0
        # is_self = calculate_distance(game_object.get_middle(), Point(width / 2, height / 2)) < accepted_radius

        if is_self:
            own_object = game_object
        else:
            other_objects.append(game_object)

    if own_object is None:
        own_object = FakeGameObject(4400, width / 2, height / 2, other_objects[0].contour)

    return own_object, other_objects


def pre_process_image(image_to_process):
    # image_to_process = cv2.cvtColor(image_to_process, cv2.COLOR_BGR2GRAY)
    image_to_process = cv2.cvtColor(image_to_process, cv2.COLOR_BGRA2BGR)
    image_to_process = cv2.medianBlur(image_to_process, 15)
    # image_to_process = cv2.equalizeHist(image_to_process)
    # image_to_process = cv2.cvtColor(image_to_process, cv2.COLOR_GRAY2BGR)

    return image_to_process


def beat_the_fcking_game(image_to_process):
    image_to_process = pre_process_image(image_to_process)

    own_object, other_objects = get_objects(image_to_process)
    # print(str(own_object.mass))
    # mouse_offset_x, mouse_offset_y, most_attractive_object = get_mouse_direction(own_object, other_objects,
    #                                                                              cv2.cvtColor(shape_mask.copy(),
    #                                                                                           cv2.COLOR_GRAY2BGR))

    # if most_attractive_object is not None:
    #     print()
    # print(most_attractive_object.attractiveness)
    # imagecpy = image_to_process.copy()
    # imagecpy = cv2.cvtColor(imagecpy, cv2.COLOR_GRAY2BGR)
    # cv2.drawContours(imagecpy, [most_attractive_object.contour], -1, (0, 255, 0), 2)
    # cv2.putText(imagecpy, str(most_attractive_object.attractiveness),
    #             (most_attractive_object.get_middle().x, most_attractive_object.get_middle().y),
    #             cv2.FONT_HERSHEY_SIMPLEX, 1, (200, 0, 0), 3, cv2.LINE_AA)

    # cv2.imshow("objects", imagecpy)
    # cv2.waitKey(0)
    # else:
    #     print("Lost myself")

    x, y = pyautogui.size()
    # pyautogui.moveTo(x / 4 + mouse_offset_x, y / 2 + mouse_offset_y)


def start():
    pyautogui.FAILSAFE = False
    pyautogui.PAUSE = 0.0
    while True:
        img = FrameGrabbing.grab_frame()
        img = np.array(img)
        start_time = time.time()
        beat_the_fcking_game(img)
        end_time = time.time()
        duration = end_time - start_time
        print("Loop duration: " + str(duration))


if __name__ == '__main__':
    start()
