import cv2

from Point import Point


class GameObject:

    def __init__(self, mass, contour):
        self.mass = mass
        self.contour = contour
        self.attractiveness = 0
        self.middle_point = None
        self.middle_point = self.get_middle()
        self.is_zersplitterer = False

    def get_middle(self):
        if self.middle_point is not None:
            return self.middle_point
        else:
            moments = cv2.moments(self.contour)
            if moments["m00"] == 0:
                cX = moments["m10"]
                cY = moments["m01"]
            else:
                cX = int(moments["m10"] / moments["m00"])
                cY = int(moments["m01"] / moments["m00"])
            return Point(cX, cY)
