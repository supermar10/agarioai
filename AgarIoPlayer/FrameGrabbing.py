 import mss


def grab_frame():
    with mss.mss() as sct:
        monitor = {"top": 140, "left": 0, "width": 1920, "height": 1080 - 140}

        # Get raw pixels from the screen, save it to a Numpy array
        return sct.grab(monitor)
        # return numpy.array(sct.grab(sct.monitors[1]))

        # sct_img = Image.frombytes('RGB', sct_img.size, sct_img.bgra, 'raw', 'BGRX').tobytes()

        # img = Image.new("RGB", sct_img.size)
        # pixels = zip(sct_img.raw[2::4], sct_img.raw[1::4], sct_img.raw[0::4])
        # img.putdata(list(pixels))
        #
        # return img
